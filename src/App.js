import './App.css';
import Header from './components/Header/Header';
import PhotoListView from './components/PhotoListView/PhotoListView';

function App() {
  return (
    <div className="App">
      <Header />
      <PhotoListView />
    </div>
  );
}

export default App;
