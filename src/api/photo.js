import axios from 'axios';

const url = 'https://api.unsplash.com/photos/';
const clientId = '6c446b49b72a4c559d9b9d67183d5c1de1981d16f309063c3b994086e6ce1a26';

export const getPhotos = async (page = 1, count = 10) => {
  try {
    const response = await axios.get(url, {
      params: {
        client_id: clientId,
        page,
        count,
      }
    });

    return [true, response];
  } catch (error) {
    return [false, error];
  }
}