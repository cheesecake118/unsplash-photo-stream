import React from 'react';
import './style.css';

const Header = () => {
  return (
    <div className="header">
      <h1>Photo Stream</h1>
      <h6 className="detail">FROM UNSPLASH</h6>
    </div>
  );
};

export default Header;
