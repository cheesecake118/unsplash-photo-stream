import React from 'react';
import './style.css';

const PhotoItem = ({
  url,
  alt,
  id,
  index,
  description,
}) => {
  return (
    <div key={`${id}_${index}`}>
      <img alt={alt} src={url} />
      <h4>{description || alt || "Free from Unsplash"}</h4>
    </div>
  );
};

export default PhotoItem;
