import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import { getPhotos } from '../../api/photo';
import PhotoItem from '../PhotoItem/PhotoItem';
import './style.css';

const PhotoListView = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  const [photos, setPhotos] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    loadPhotos();
  }, []);

  const loadPhotos = async (count = 10) => {
    const res = await getPhotos(page + 1, count);
    if (res[0] && res[1]?.data) {
      setPhotos([...photos, ...res[1].data]);
      setIsLoaded(true);
      setPage(page + 1);
    }
  }

  return (
    <div>
      {photos.length === 0 ? (
        <h4>No photos available</h4>
      ) : (
        <InfiniteScroll
          dataLength={photos}
          next={() => loadPhotos(10)}
          hasMore={true}
          loader={<h4>Getting more for you...</h4>}
        >
          <ResponsiveMasonry>
              <Masonry>
                {isLoaded ?
                  photos.map((photo, index) => (
                    <PhotoItem
                      index={index}
                      url={photo.urls.regular}
                      id={photo.id}
                      alt={photo.alt_description}
                      description={photo.description}
                    />
                  )): ""}
              </Masonry>
          </ResponsiveMasonry>
        </InfiniteScroll>
      )}
    </div>
  );
};

export default PhotoListView;
